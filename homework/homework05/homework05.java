package homework05;

/*
Реализовать программу на Java, которая для последовательности оканчивающихся,
чисел на -1 выведет самую минимальную цифру, встречающуюся среди чисел последовательности.

Например:
345
298
456
-1

Ответ: 2
*/

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class homework05 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int minDigit = 9;
        while (true) {
            var line = reader.readLine();
            int number = -1;
            try {
                number = Integer.parseInt(line);
            } catch (NumberFormatException e) {
                System.out.println("мин число");
            }
            if (number == -1) {
                break;
            }
            if (number == 0) {
                minDigit = 0;
                continue;
            }
            while (number != 0) {
                var digit = number % 10;
                if (digit < minDigit) {
                    minDigit = digit;
                }
                number /= 10;
            }
        }
        System.out.println(minDigit);

    }
}

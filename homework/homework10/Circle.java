package homework10;

public class Circle extends Ellipse implements Moving {
    private double radius;


    public Circle(int coordX, int coordY, double radius) {
        super(coordX, coordY, radius, radius);
        this.radius = radius;
        System.out.println("Создан круг с координатами: x = " + this.coordX + ", y = " + this.coordY + " и радиусом = " + radius);
    }

    @Override
    public void moveToThisCord(int setCoordX, int setCoordY) {
        this.coordX = setCoordX;
        this.coordY = setCoordY;
        System.out.println("Перемещен круг на следующие координаты: x = " + this.coordX + ", y = " + this.coordY);
    }

    @Override
    public double getPerimeter() {
        return 4 * (Math.PI * this.radius);
    }


}

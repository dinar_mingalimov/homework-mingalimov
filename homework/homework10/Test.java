package homework10;

public class Test {
    public static void test() {
        Moving[] movingFigures = new Moving[2];

        Circle circle = new Circle(0, 0, 2.0);
        Square square = new Square(0, 0, 4);

        movingFigures[0] = circle;
        movingFigures[1] = square;

        for (int i = 0; i < movingFigures.length; i++) {
            movingFigures[i].moveToThisCord(10, 10);
        }
        System.out.println("Проверка условия");

        if (circle.getCoordX() == square.getCoordX() && circle.getCoordY() == square.getCoordY()) {
            String nameSquare = square.getClass().getSimpleName();
            String nameCircle = circle.getClass().getSimpleName();
            System.out.printf("Координаты %s и %s равны по X = %d и по Y = %d\n", nameCircle, nameSquare, circle.getCoordX(), circle.getCoordY());
            System.out.println("А значит, тест пройден");
        } else {
            System.out.println("Тест не пройден");
        }
    }
}

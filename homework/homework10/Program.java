package homework10;

/*
Сделать класс Figure из задания 09 абстрактным.
Определить интерфейс, который позволит перемещать фигуру на заданные координаты.
Данный интерфейс должны реализовать только классы Circle и Square.
В Main создать массив "перемещаемых" фигур и сдвинуть все их в одну конкретную точку.
 */

import java.util.Scanner;

public class Program {
    public static void main(String[] args) {

        String test = "";
        Scanner scanner = new Scanner(System.in);

        while (!((test.equals("y")) || (test.equals("n")))) {
            System.out.print("Запуск теста? y / n: ");
            test = scanner.next().toLowerCase();
        }
        if (test.equals("y")) {
            Test.test();
            return;
        }

        Moving[] movingFigures = new Moving[2];

        Circle circle = new Circle(0, 0, 2.0);
        Square square = new Square(0, 0, 4);

        movingFigures[0] = circle;
        movingFigures[1] = square;

        for (int i = 0; i < movingFigures.length; i++) {
            movingFigures[i].moveToThisCord(2, 3);
        }
    }
}

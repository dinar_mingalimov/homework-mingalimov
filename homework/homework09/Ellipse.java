package homework09;

public class Ellipse extends Figure {
    protected double bigRadius;
    private double smallRadius;

    public Ellipse(int coordX, int coordY, double bigRadius, double smallRadius) {
        super(coordX, coordY);
        this.bigRadius = bigRadius;
        this.smallRadius = smallRadius;
    }

    @Override
    public double getPerimeter() {
        return 2 * Math.PI * Math.sqrt((bigRadius * bigRadius + smallRadius * smallRadius) / 2); // P= 2π√(a2+b2)/2
    }
}


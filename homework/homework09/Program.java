package homework09;
/*
Сделать класс Figure, у данного класса есть два поля - x и y координаты.
Классы Ellipse и Rectangle должны быть потомками класса Figure.
Класс Square - потомок класса Rectangle, Circle - потомок класса Ellipse.
В классе Figure предусмотреть метод getPerimeter(), который возвращает 0.
Во всех остальных классах он должен возвращать корректное значение.

 */

public class Program {
    public static void main(String[] args) {
        Figure[] figures = new Figure[4];

        figures[0] = new Ellipse(0, 0, 3, 2);
        figures[1] = new Rectangle(0, 0, 2, 4);
        figures[2] = new Circle(0, 0, 5);
        figures[3] = new Square(0, 0, 5);
        for (int indexFigure = 0; indexFigure < figures.length; indexFigure++) {
            System.out.println("Периметр фигуры " + figures[indexFigure].getClass().getSimpleName() + " = " + figures[indexFigure].getPerimeter());
        }
    }

}

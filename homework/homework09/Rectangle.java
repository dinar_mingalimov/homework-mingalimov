package homework09;

public class Rectangle extends Figure {
    private double sideA;
    private double sideB;

    public Rectangle(int coordX, int coordY, double sideA, double sideB) {
        super(coordX, coordY);
        this.sideA = sideA;
        this.sideB = sideB;
    }

    @Override
    public double getPerimeter() {
        return sideA + sideA + sideB + sideB;
    }
}


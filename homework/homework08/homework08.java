package homework08;

/*
На вход подается информация о людях в количестве 10 человек (имя - строка, вес - вещественное число).
Считать эти данные в массив объектов.
Вывести в отсортированном по возрастанию веса порядке.

 */

public class homework08 {
    public static void main(String[] args) {
        Human[] humans = new Human[9];

        for (int i = 0; i < humans.length; i++) {

            humans[i] = new Human("User" + i, (int) (Math.random() * 100));
        }
        selectionSort(humans);
        for (Human human : humans) {
            System.out.println(human.getName() + "  " + human.getWeight() + "  ");
        }
    }

    public static void selectionSort(Human[] humans) {
        for (int i = 0; i < humans.length - 1; i++) {
            int minIndex = i;
            for (int j = i; j < humans.length; j++) {
                if (humans[j].getWeight() < humans[minIndex].getWeight()) {
                    minIndex = j;
                }
            }
            Human temp = humans[i];
            humans[i] = humans[minIndex];
            humans[minIndex] = temp;
        }
    }
}

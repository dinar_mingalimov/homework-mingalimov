package homework07;

import java.util.HashMap;
import java.util.Map;

/*

а вход подается последовательность чисел, оканчивающихся на -1.
Необходимо вывести число, которе присутствует в последовательности минимальное количество раз.
Гарантируется:
Все числа в диапазоне от -100 до 100.
Числа встречаются не более 2 147 483 647-раз каждое.
Сложность алгоритма - O(n)

 */
public class homework07 {
    public static void main(String[] args) {
        int[] numbers = {1, 2, 3, 3, 4, 3, 5, 1, 5, 2};

        Map<Integer, Integer> numberCount = new HashMap<>();
        for (int i : numbers) {
            numberCount.merge(i, 1, Integer::sum);
        }
        int minCount = Integer.MAX_VALUE;
        int minNumber = -1;
        for (Map.Entry<Integer, Integer> entry : numberCount.entrySet()) {
            var count = entry.getValue();
            if (count < minCount) {
                minCount = count;
                minNumber = entry.getKey();
            }
        }
        System.out.println(minNumber);
    }
}
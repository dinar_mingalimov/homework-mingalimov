package homework06;

import java.util.Arrays;

/*
Реализовать функцию, принимающую на вход массив и целое число.
Данная функция должна вернуть индекс этого числа в массиве. Если число в массиве отсутствует - вернуть -1.
Реализовать процедуру, которая переместит все значимые элементы влево, заполнив нулевые, например:

        было:
        34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20

        стало
        34, 14, 15, 18, 1, 20, 0, 0, 0, 0, 0
 */
public class homework06 {
    public static void main(String[] args) {
        int[] a = {34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};
        moveNonZeroElementsToLeft(a);
        System.out.println(Arrays.toString(a));

    }

    static int index0f(int[] array, int n) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == n) {
                return i;
            }
        }
        return -1;
    }

    static void moveNonZeroElementsToLeft(int[] array) {
        int indexOfZero = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] != 0) {
                if (indexOfZero < i) {
                    array[indexOfZero] = array[i];
                    array[i] = 0;

                }
                indexOfZero++;
            }
        }
    }
}

package homework13;

import java.util.Arrays;

public class Test {
    public static void test(int[] array) {

        int[] resultArray = Sequence.filter(array, Program.condition);

        System.out.println("Input array:");
        System.out.println(Arrays.toString(array));

        System.out.println("Array even numbers:");

        int[] evenArray = new int[array.length];
        int countEvenValuesInArray = 0;

        for (int j : array) {

            if (Program.isEven(j)) {
                evenArray[countEvenValuesInArray] = j;
                countEvenValuesInArray++;
            }
        }
        evenArray = Arrays.copyOf(evenArray, countEvenValuesInArray);
        System.out.println(Arrays.toString(evenArray));

        System.out.println("Array with even sum of digits :");

        int countEvenSumDigits = 0;
        int[] resultTestArray = new int[evenArray.length];

        for (int j : evenArray) {

            if (Program.isSumDigitsEven(j)) {
                resultTestArray[countEvenSumDigits] = j;
                countEvenSumDigits++;
            }

        }
        resultTestArray = Arrays.copyOf(resultTestArray, countEvenSumDigits);
        System.out.println(Arrays.toString(resultTestArray));

        System.out.println("Result with Sequence.filter:\n" + Arrays.toString(resultArray));

        if (resultArray.length == resultTestArray.length) {

            if (resultArray.length == 0) {
                System.out.println("Arrays are empty");
            } else {

                for (int i = 0; i < resultArray.length; i++) {

                    if (!(resultArray[i] == resultTestArray[i])) {
                        System.out.println("Test unsuccessful.");
                        return;
                    }
                }
                System.out.println("Arrays are equal. Test successful.");
            }
        } else {
            System.out.println("Test unsuccessful.");
        }
    }
}


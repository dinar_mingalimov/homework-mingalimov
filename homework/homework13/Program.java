package homework13;
/*
Предусмотреть функциональный интерфейс

interface ByCondition {
	boolean isOk(int number);
}

Реализовать в классе Sequence метод:

public static int[] filter(int[] array, ByCondition condition) {
	...
}

Данный метод возвращает массив, который содержит элементы, удовлетворяющиие логическому выражению в condition.

В main в качестве condition подставить:

- проверку на четность элемента
- проверку, является ли сумма цифр элемента четным числом.

 */

import java.util.Arrays;
import java.util.Scanner;

public class Program {

    static ByCondition condition = number -> isEven(number) && isSumDigitsEven(number);

    public static void main(String[] args) {
        String isTest = "";
        Scanner scanner = new Scanner(System.in);

        while (!(isTest.equals("y") || isTest.equals("n"))) {
            System.out.print("Test task? y / n: ");
            isTest = scanner.next();
            isTest = isTest.toLowerCase();
        }

        int[] inArray;

        if (isTest.equals("y")) {
            inArray = new int[]{0, 15, 54, 46, 257, 468, 473, 25, 255, 114, 32, 79};
            Test.test(inArray);
            return;
        }

        inArray = getArray(20);

        System.out.print("Random array: ");
        System.out.println(Arrays.toString(inArray));

        int[] resultArray = Sequence.filter(inArray, condition);

        System.out.print("Result: ");
        System.out.println(Arrays.toString(resultArray));
    }


    public static int[] getArray(int size) {

        int[] array = new int[size];
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 1000);
        }

        return array;
    }


    public static boolean isEven(int value) {
        return (value % 2 == 0);
    }

    public static boolean isSumDigitsEven(int value) {
        int sumDigits = 0;

        while (value != 0) {
            sumDigits += value % 10;
            value /= 10;
        }
        return isEven(sumDigits);
    }
}


package homework13;

import java.util.Arrays;

public class Sequence {

    public static int[] filter(int[] array, ByCondition condition) {

        int[] outArray = new int[array.length];


        int countMatchElements = 0;

        for (int numFromArray : array) {

            if (condition.isOk(numFromArray)) {
                outArray[countMatchElements] = numFromArray;
                countMatchElements++;
            }
        }

        outArray = Arrays.copyOf(outArray, countMatchElements);

        return outArray;
    }
}

